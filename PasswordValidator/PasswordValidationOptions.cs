﻿using System;

namespace PasswordValidator
{
    public class PasswordValidationOptions
    {
        public int MinLength { get; set; }
        public bool ShouldHaveUpperCase { get; set; }
        public bool ShouldHaveLowerCase { get; set; }
        public bool ShouldHaveNumber { get; set; }
        public bool ShouldHaveSymbol { get; set; }
        public Func<PasswordStats, bool> WeakMatch { get; set; }
        public Func<PasswordStats, bool> OkMatch { get; set; }
        public Func<PasswordStats, bool> StrongMatch { get; set; }
    }
}

﻿using System;

namespace PasswordValidator
{
    public enum PasswordStrength
    {
        Invalid = -1,
        Unknown = 0,
        Weak = 1,
        Ok = 2,
        Strong = 3
    }
}

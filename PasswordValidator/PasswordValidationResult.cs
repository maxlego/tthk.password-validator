﻿namespace PasswordValidator
{
    public class PasswordValidationResult
    {
        public bool IsValid { get; set; }
        public string[] Errors { get; set; }
        public PasswordStrength Strength { get; set; }
    }
}

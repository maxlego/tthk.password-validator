﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace PasswordValidator
{
    public class Validator
    {
        private static Regex lowerCaseRegex = new Regex(@"[a-z]");
        private static Regex upperCaseRegex = new Regex(@"[A-Z]");
        private static Regex numberRegex = new Regex(@"[0-9]");
        private static Regex symbolRegex = new Regex(@"[^a-zA-Z0-9]");

        private readonly PasswordValidationOptions options;

        public Validator(PasswordValidationOptions options)
        {
            this.options = options;
        }

        public PasswordValidationResult Validate(string password)
        {
            var errors = new List<string>();
            var stats = GetStats(password);

            if (stats.Length < options.MinLength)
            {
                errors.Add("Password too short");
            }
            if (options.ShouldHaveLowerCase && !stats.HasLowerCase)
            {
                errors.Add("Should have lower case");
            }
            if (options.ShouldHaveUpperCase && !stats.HasUpperCase)
            {
                errors.Add("Should have upper case");
            }
            if (options.ShouldHaveNumber && !stats.HasNumber)
            {
                errors.Add("Should have number");
            }
            if (options.ShouldHaveSymbol && !stats.HasSymbol)
            {
                errors.Add("Should have symbol");
            }

            var strength = GetStrength(stats, options);
            return new PasswordValidationResult
            {
                IsValid = errors.Count == 0,
                Errors = errors.ToArray(),
                Strength = strength
            };
        }

        public PasswordStats GetStats(string password)
        {
            if (password == null)
            {
                password = string.Empty;
            }

            return new PasswordStats
            {
                Length = password.Length,
                HasLowerCase = lowerCaseRegex.IsMatch(password),
                HasUpperCase = upperCaseRegex.IsMatch(password),
                HasNumber = numberRegex.IsMatch(password),
                HasSymbol = symbolRegex.IsMatch(password)
            };
        }

        public PasswordStrength GetStrength(PasswordStats stats, PasswordValidationOptions options)
        {
            if (options.StrongMatch != null && options.StrongMatch(stats))
            {
                return PasswordStrength.Strong;
            }
            if (options.OkMatch != null && options.OkMatch(stats))
            {
                return PasswordStrength.Ok;
            }
            if (options.WeakMatch != null && options.WeakMatch(stats))
            {
                return PasswordStrength.Weak;
            }

            return PasswordStrength.Unknown;
        }
    }

    public class PasswordStats
    {
        public int Length { get; set; }
        public bool HasLowerCase { get; set; }
        public bool HasUpperCase { get; set; }
        public bool HasNumber { get; set; }
        public bool HasSymbol { get; set; }
    }
}

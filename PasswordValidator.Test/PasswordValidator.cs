using System;
using Xunit;

namespace PasswordValidator.Test
{
    public class PasswordValidator
    {
        [Fact]
        public void Should_ValidateGivenLength()
        {
            var validator = new Validator(
                new PasswordValidationOptions
                {
                    MinLength = 5
                });

            TestPassword(validator, validPassword: "erte5", invalidPassword: "erte");
        }

        [Fact]
        public void Should_ValidateIfPasswordContainsLowerCaseLetters()
        {
            var validator = new Validator(
                new PasswordValidationOptions
                {
                    ShouldHaveLowerCase = true
                });

            TestPassword(validator, validPassword: "erte5", invalidPassword: "ASDF");
        }

        [Fact]
        public void Should_ValidateIfPasswordContainsUpperCaseLetters()
        {
            var validator = new Validator(
                new PasswordValidationOptions
                {
                    ShouldHaveUpperCase = true
                });

            TestPassword(validator, validPassword: "ASDF", invalidPassword: "asdf");
        }

        [Fact]
        public void Should_ValidateIfPasswordContainsNumber()
        {
            var validator = new Validator(
                new PasswordValidationOptions
                {
                    ShouldHaveNumber = true
                });

            TestPassword(validator, validPassword: "2345", invalidPassword: "asdf");
        }

        [Fact]
        public void Should_ValidateIfPasswordContainsSymbol()
        {
            var validator = new Validator(
                new PasswordValidationOptions
                {
                    ShouldHaveSymbol = true
                });

            TestPassword(validator, validPassword: ".", invalidPassword: "asdf");
        }

        private void TestPassword(Validator validator, string validPassword, string invalidPassword)
        {
            var res = validator.Validate(validPassword);
            Assert.True(res.IsValid);

            res = validator.Validate(invalidPassword);
            Assert.False(res.IsValid);
        }

        [Fact]
        public void Should_ValidateIfPasswordIsWeak()
        {
            var validator = new Validator(GetStrengthOptions());

            var res = validator.Validate("erte5");
            Assert.Equal(PasswordStrength.Weak, res.Strength);
        }

        [Fact]
        public void Should_ValidateIfPasswordIsOK()
        {
            var validator = new Validator(GetStrengthOptions());

            var res = validator.Validate("erte55");
            Assert.Equal(PasswordStrength.Ok, res.Strength);
        }

        [Fact]
        public void Should_ValidateIfPasswordIsStrong()
        {
            var validator = new Validator(GetStrengthOptions());

            var res = validator.Validate("Xerte55!");
            Assert.Equal(PasswordStrength.Strong, res.Strength);
        }

        private PasswordValidationOptions GetStrengthOptions()
        {
            return new PasswordValidationOptions
            {
                StrongMatch = x => x.HasLowerCase && x.HasUpperCase && x.HasNumber && x.HasSymbol && x.Length >= 8,
                OkMatch = x => (x.HasLowerCase || x.HasUpperCase) && (x.HasNumber || x.HasSymbol) && x.Length >= 6,
                WeakMatch = x => true,
            };
        }
    }
}
